package com.example.nds.primroza.main;

import com.example.nds.primroza.classes.Profile;

public interface IWorkWithShared {
    Profile isLoggined();
    void setLoginStatus(Profile profile);
}
