package com.example.nds.primroza.base.events;

public class Event {
    protected int actionCode;

    public Event(int actionCode){
        this.actionCode = actionCode;
        }

    public int getActionCode() {
        return actionCode;
    }

    public void setActionCode(int actionCode) {
        this.actionCode = actionCode;
    }
}
