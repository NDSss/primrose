package com.example.nds.primroza.map.bottomsheet;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.nds.primroza.R;

import butterknife.ButterKnife;

/**
 * Created by Karina on 02.01.2019.
 */
public class MapBottomSheet extends LinearLayout {

    public MapBottomSheet(Context context) {
        super(context);
    }

    public MapBottomSheet(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MapBottomSheet(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        inflate(context, R.layout.view_map_bottom_sheet, this);
        ButterKnife.bind(this);
    }

    public void setData() {

    }

    private void expland() {
//        tvBranchExtraInformaton.setMaxLines(10);
    }

    private void hide() {
//        tvBranchExtraInformaton.setMaxLines(2);
    }

    public interface IRemoveView {
        void removeView();

        void getLocation();
    }

}
