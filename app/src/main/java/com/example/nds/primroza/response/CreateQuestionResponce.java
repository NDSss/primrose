package com.example.nds.primroza.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.nds.primroza.classes.MainProducts;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

public class CreateQuestionResponce implements Parcelable {
    @SerializedName("question_id")
    private int question_id;

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.question_id);
    }

    public CreateQuestionResponce() {
    }

    protected CreateQuestionResponce(Parcel in) {
        this.question_id = in.readInt();
    }

    public static final Parcelable.Creator<CreateQuestionResponce> CREATOR = new Parcelable.Creator<CreateQuestionResponce>() {
        @Override
        public CreateQuestionResponce createFromParcel(Parcel source) {
            return new CreateQuestionResponce(source);
        }

        @Override
        public CreateQuestionResponce[] newArray(int size) {
            return new CreateQuestionResponce[size];
        }
    };
}
