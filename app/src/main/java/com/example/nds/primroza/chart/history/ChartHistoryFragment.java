package com.example.nds.primroza.chart.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.base.eventbus.IEventBusObserver;
import com.example.nds.primroza.base.events.Event;

public class ChartHistoryFragment extends Fragment implements IEventBusObserver {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get(getContext()).getEventBus().addObserver(this::onEvent);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chart_history,container,false);
        return view;
    }

    @Override
    public void onEvent(Event event) {

    }
}
