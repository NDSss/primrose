package com.example.nds.primroza.base;

public interface IOnAdapterItemSelected {
    void onItemSelected(int position);
}
