package com.example.nds.primroza.base;

import android.widget.EditText;

public interface IBaseEditDialog {
    void edit(String data);
}
