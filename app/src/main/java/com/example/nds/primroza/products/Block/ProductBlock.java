package com.example.nds.primroza.products.Block;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.classes.ProductList;

import java.util.ArrayList;

public class ProductBlock extends RelativeLayout {

    private View rootContainer;
    private TextView tvTitle;
    private TextView tvDescription;
    private RecyclerView recyclerView;
    private ProductBlockAdapter adapter;
    private OnProductBlockItemClickedListener onProductBlockItemClickedListener;

    private ProductList products;
    private int position;

    public ProductBlock(Context context,int position, ProductList products) {
        super(context);
        this.position = position;
        this.products = products;
        init(context);
    }

    public int getPosition() {
        return position;
    }

    public void setOnProductBlockItemClickedListener(OnProductBlockItemClickedListener onProductBlockItemClickedListener) {
        this.onProductBlockItemClickedListener = onProductBlockItemClickedListener;
        if(adapter!=null){
            adapter.setOnProductBlockItemClickedListener(onProductBlockItemClickedListener);
        }
    }

    private void init(Context context){
        LayoutInflater inflater = LayoutInflater.from(context);
        rootContainer = inflater.inflate(R.layout.block_product,this);
        tvTitle = (TextView) rootContainer.findViewById(R.id.tv_block_product_title);
        tvTitle.setText(products.getName());
        tvDescription = (TextView) rootContainer.findViewById(R.id.tv_block_product_description);
        tvDescription.setText(products.getName());
        recyclerView = (RecyclerView) rootContainer.findViewById(R.id.rv_block_product);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ProductBlockAdapter(position,products.getProducts());
        recyclerView.setAdapter(adapter);
    }

    public interface OnProductBlockItemClickedListener{
        void itemClicked(int blockPosition, int elementPosition);
    }

}
