package com.example.nds.primroza.splash;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.classes.City;

import java.util.ArrayList;
import java.util.List;

public class CitiesSpinnerAdapter extends ArrayAdapter {

    ArrayList<City> cities;

    public CitiesSpinnerAdapter(@NonNull Context context, int resource, @NonNull ArrayList<City> objects) {
        super(context, resource, objects);
        cities = objects;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return cities.get(position).getName();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.item_spinner_cities, null);
        }
            TextView tv = (TextView) convertView;
            tv.setText(cities.get(position).getName());
        return convertView;
    }
}
