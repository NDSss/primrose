package com.example.nds.primroza.classes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.annotations.JsonAdapter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

@JsonAdapter(CityResponce.CitiesDeserializer.class)
public class CityResponce implements Parcelable {
    private ArrayList<City> cities;

    public ArrayList<City> getCities() {
        return cities;
    }

    public void setCities(ArrayList<City> cities) {
        this.cities = cities;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.cities);
    }

    public CityResponce() {
    }

    protected CityResponce(Parcel in) {
        this.cities = in.createTypedArrayList(City.CREATOR);
    }

    public static final Parcelable.Creator<CityResponce> CREATOR = new Parcelable.Creator<CityResponce>() {
        @Override
        public CityResponce createFromParcel(Parcel source) {
            return new CityResponce(source);
        }

        @Override
        public CityResponce[] newArray(int size) {
            return new CityResponce[size];
        }
    };

    public static class CitiesDeserializer implements JsonDeserializer<CityResponce> {

        @Override
        public CityResponce deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            CityResponce responce = new CityResponce();
            ArrayList<City> cities = new ArrayList<>();
            for(int i = 0;i< json.getAsJsonArray().size();i++){
                cities.add(context.deserialize(json.getAsJsonArray().get(i),City.class));
            }
            responce.setCities(cities);
            return responce;
//            MainProducts getProducts = new MainProducts();
//            getProducts.setCategories(new ArrayList<>());
//            JsonElement root = new JsonParser().parse(json.toString());
//            JsonObject object = root.getAsJsonObject().getAsJsonObject();
//            ArrayList<ProductList> categoties = new ArrayList<>();
//            for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
//                ArrayList<Product> arrayProdusts =(context.deserialize(entry.getValue(), ArrayList.class));
//                ProductList productList = new ProductList();
//                ArrayList<Product> parsedProdusts = new ArrayList<>();
//                JsonArray jar = new Gson().toJsonTree(arrayProdusts).getAsJsonArray();
//                for(int i =0; i < jar.size(); i++){
//                    Product product = new Gson().fromJson(jar.get(i), Product.class);
//                    parsedProdusts.add(product);
//                }
//                productList.setProducts(parsedProdusts);
//                productList.setName(entry.getKey());
//                categoties.add(productList);
//            }
//            getProducts.setCategories(categoties);
        }
    }
}
