package com.example.nds.primroza.app;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.nds.primroza.base.eventbus.EventBusController;
import com.example.nds.primroza.classes.City;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.di.DaggerActivityComponent;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    private static final String TAG = "App";
    private Retrofit retrofit;
    private EventBusController eventBus;
    private City city;
    private ActivityComponent activityComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        initRetrofit();
        eventBus = new EventBusController();
        Fresco.initialize(this);
//        FirebaseApp.initializeApp(this);
//        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
    }
    private void initRetrofit(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        //create an instance of Retrofit
        retrofit = new Retrofit.Builder()
                .baseUrl(Urls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public EventBusController getEventBus(){
        return eventBus;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public ActivityComponent getActivityComponent(){
        if(activityComponent==null){
            activityComponent = DaggerActivityComponent.builder().build();
        }
        return activityComponent;
    }

}
