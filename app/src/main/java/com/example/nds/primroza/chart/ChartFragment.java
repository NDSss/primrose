package com.example.nds.primroza.chart;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.base.eventbus.IEventBusObserver;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.chart.current.ChartCurrentFragment;
import com.example.nds.primroza.chart.history.ChartHistoryFragment;
import com.example.nds.primroza.classes.Product;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChartFragment extends Fragment implements IEventBusObserver {

    private String TAG = "ChartFragment";
    @BindView(R.id.tl_fragment_chart_tabs)
    TabLayout tlFragmentChartTabs;
    @BindView(R.id.vp_fragment_chart_container)
    ViewPager vpFragmentChartContainer;
    Unbinder unbinder;
    private ChartCurrentFragment chartCurrentFragment;
    private ChartHistoryFragment chartHistoryFragment;
    private ChartViewPagerAdapter adapter;
    private int count;
    private ArrayList<Product> current;

    public static ChartFragment newInstance(int count){
        ChartFragment fragment = new ChartFragment();
        Bundle args = new Bundle();
        args.putInt("int",count);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        Log.d(TAG, "setArguments: ");
        count = args.getInt(ChartCurrentFragment.ITEMS_COUNT);
//        createFragments();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        App.get(getContext()).getEventBus().addObserver(this::onEvent);
        createFragments();
    }

    private void createFragments(){
        Log.d(TAG, "createFragments: ");
        chartCurrentFragment = new ChartCurrentFragment();
        chartHistoryFragment = new ChartHistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ChartViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(chartCurrentFragment,"current");
        adapter.addFragment(chartHistoryFragment,"history");
        vpFragmentChartContainer.setAdapter(adapter);
        tlFragmentChartTabs.setupWithViewPager(vpFragmentChartContainer);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onEvent(Event event) {
        Log.d(TAG, "onEvent: ");
    }
}
