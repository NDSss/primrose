package com.example.nds.primroza.di;

import com.example.nds.primroza.chart.current.ChartCurrentFragment;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.loginregistration.LoginActivity;
import com.example.nds.primroza.main.MainActivity;
import com.example.nds.primroza.products.ProductFragment;
import com.example.nds.primroza.products.order.OrderActivity;
import com.example.nds.primroza.products.preview.ProductPreviewActivity;
import com.example.nds.primroza.profile.ProfileFragment;
import com.example.nds.primroza.splash.SplashActivity;
import com.example.nds.primroza.support.SupportQuestionsActivity;
import com.example.nds.primroza.support.supportChatActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);
    void inject(MainActivity mainActivity);
    void inject(ProductPreviewActivity productPreviewActivity);
    void inject(OrderActivity orderActivity);
    void inject(ChartCurrentFragment chartCurrentFragment);
    void inject(ProductFragment productFragment);
    void inject(ProfileFragment profileFragment);
    void inject(LoginActivity loginActivity);
    void inject(SupportQuestionsActivity supportQuestionsActivity);
    void inject(supportChatActivity supportChatActivity);
}
