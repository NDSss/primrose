package com.example.nds.primroza.loginregistration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.Utils;
import com.example.nds.primroza.classes.Profile;
import com.github.pinball83.maskededittext.MaskedEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class SignInActivity extends AppCompatActivity {
    public static final String PROFILE = "SignInActivity.PROFILE";
    @BindView(R.id.et_sign_in_name)
    EditText etSignInName;
    @BindView(R.id.til_sign_in_name)
    TextInputLayout tilSignInName;
    @BindView(R.id.et_sign_in_phone)
    MaskedEditText etSignInPhone;
    @BindView(R.id.til_sign_in_phone)
    TextInputLayout tilSignInPhone;
    @BindView(R.id.et_sign_in_email)
    EditText etSignInEmail;
    @BindView(R.id.til_sign_in_email)
    TextInputLayout tilSignInEmail;
    @BindView(R.id.et_sign_in_city)
    EditText etSignInCity;
    @BindView(R.id.til_sign_in_city)
    TextInputLayout tilSignInCity;
    @BindView(R.id.btn_sign_in_complete)
    Button btnSignInComplete;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        setListeners();
    }

    private void setListeners(){
        btnSignInComplete.setOnClickListener(v->completeRegistration());
        List<io.reactivex.Observable<Boolean>> observableList = new ArrayList<>();
        observableList.add(Utils.getPhoneWatcher(etSignInPhone,tilSignInPhone));
        observableList.add(Utils.notEmptyObserver(etSignInName,3,tilSignInName));
        observableList.add(Utils.notEmptyObserver(etSignInCity,4,tilSignInCity));
        observableList.add(Utils.patternMatcherObserver(etSignInEmail, Patterns.EMAIL_ADDRESS,tilSignInEmail));
        Disposable validationDisposable = io.reactivex.Observable.combineLatest(observableList,objects -> {
            boolean state = true;
            for(Object object: objects){
                state &= (Boolean) object;
            }
            return state;
        }).subscribe(s->setBtnCompleteEnabled(s));
        mCompositeDisposable.add(validationDisposable);
    }

    private void setBtnCompleteEnabled(boolean isEnabled){
        btnSignInComplete.setEnabled(isEnabled);
    }

    private void completeRegistration(){
        Profile profile = new Profile();
        profile.setLogin(true);
        profile.setMail(etSignInEmail.getText().toString());
        profile.setCityName(etSignInCity.getText().toString());
        profile.setName(etSignInName.getText().toString());
        profile.setPhone(etSignInPhone.getText().toString());
        Intent intent = new Intent();
        intent.putExtra(PROFILE,profile);
        setResult(Actions.RESULT_SIGN_IN_SIGNED,intent);
        finish();
    }

}
