package com.example.nds.primroza.app;

public class Actions {
    public static final int START_LOGIN_ACTIVITY = 100;
    public static final int START_SIGN_IN_ACTIVITY = 101;
    public static final int START_PREVIEW_ACTIVITY = 102;

    public static final int RESULT_SIGN_IN_SIGNED = 1000;
    public static final int RESULT_LOGIN_OK = 1001;
    public static final int RESULT_PREVIEW_TO_FAVORITE = 1002;
    public static final int RESULT_PREVIEW_TO_NON_FAVORITE = 1003;

    public static final int DIALOG_EDIT_PHONE = 2000;
    public static final int DIALOG_EDIT_ADRESS = 2001;
    public static final int DIALOG_EDIT_EXTRA_INF = 2002;
    public static final int DIALOG_EDIT_DELIVERY_DATE = 2003;
}
