package com.example.nds.primroza.chart.current;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Urls;
import com.example.nds.primroza.base.IOnAdapterItemSelected;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.products.Block.ProductBlockAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {

    private ArrayList<Product> products;
    private IOnAdapterItemSelected listener;

    public void setListener(IOnAdapterItemSelected listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(Urls.FULL_IMAGE+products.get(position).getPhotos().get(0)))
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .setResizeOptions(new ResizeOptions(150,150))
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .build();
        holder.picture.setController(controller);
        holder.tvName.setText(products.get(position).getTitle());
        holder.tvPrice.setText(products.get(position).getPrice());
        holder.tvDesc.setText(products.get(position).getDescription()==null?"Description":products.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return products==null?0:products.size();
    }

    public void setData(ArrayList<Product> products){
        this.products = products;
        notifyDataSetChanged();
    }

    public void addItem(Product product){
        if(products==null){
            products = new ArrayList<>();
        }
        products.add(product);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView picture;
        TextView tvName;
        TextView tvPrice;
        TextView tvDesc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.sdv_item_order_detail);
            tvName = itemView.findViewById(R.id.tv_item_order_detail_name);
            tvPrice = itemView.findViewById(R.id.tv_item_order_detail_price);
            tvDesc = itemView.findViewById(R.id.tv_item_order_detail_description);
            itemView.setOnClickListener(v->listener.onItemSelected(getAdapterPosition()));
        }
    }
}
