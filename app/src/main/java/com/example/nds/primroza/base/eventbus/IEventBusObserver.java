package com.example.nds.primroza.base.eventbus;

import com.example.nds.primroza.base.events.Event;

public interface IEventBusObserver {
    void onEvent(Event event);
}
