package com.example.nds.primroza.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.DataManager;
import com.example.nds.primroza.base.IBaseLoadingActivity;
import com.example.nds.primroza.base.eventbus.IEventBusObserver;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.base.events.EventsActions;
import com.example.nds.primroza.classes.City;
import com.example.nds.primroza.classes.MainProducts;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.main.IChart;
import com.example.nds.primroza.products.Block.ProductBlock;
import com.example.nds.primroza.products.preview.ProductPreviewActivity;
import com.example.nds.primroza.servises.IProductServise;

import java.util.LinkedHashMap;

import javax.inject.Inject;

import retrofit2.Callback;
import retrofit2.Response;

public class ProductFragment extends Fragment implements ProductBlock.OnProductBlockItemClickedListener,IEventBusObserver {
    private LinearLayout llContainer;
    private MainProducts mMainProducts;
    private int selectedBlock=0,selectedProduct=0;
    private IBaseLoadingActivity iLoadig;
    private City city;
    private TextView tvCity;
    @Inject
    DataManager dataManager;

    public void setCity(City city) {
        this.city = city;
    }

    public void setiLoadig(IBaseLoadingActivity iLoadig) {
        this.iLoadig = iLoadig;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get(getContext()).getActivityComponent().inject(this);
        App.get(getContext()).getEventBus().addObserver(this::onEvent);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products,container,false);
        llContainer = view.findViewById(R.id.ll_products_main_container);
        tvCity = view.findViewById(R.id.tv_fragment_product_city);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvCity.setText(city.getName());
        loadProducts();
    }

    private void handleProducts(MainProducts mainProducts){
        dataManager.setProducts(mainProducts);
        mMainProducts = dataManager.getProducts();
        for(int i = 0; i < mMainProducts.getCategories().size();i++){
            ProductBlock block = new ProductBlock(getContext(),i,mainProducts.getCategories().get(i));
            block.setOnProductBlockItemClickedListener(this::itemClicked);
            llContainer.addView(block);
        }
        iLoadig.completeLoading();
    }

    private void loadProducts() {
        iLoadig.startLoading();
        IProductServise productServise = App.get(getContext()).getRetrofit().create(IProductServise.class);
        LinkedHashMap<String,Integer> map = new LinkedHashMap<>();
        if(city!=null) {
            map.put("city", city.getId());
        } else {

            map.put("city", 1);
        }
        map.put("width",300);
        map.put("height",300);
        final retrofit2.Call<MainProducts> weatherResponceCall = productServise.getProducts(map);
        weatherResponceCall.enqueue(new Callback<MainProducts>() {
            @Override
            public void onResponse(retrofit2.Call<MainProducts> call, Response<MainProducts> response) {
                handleProducts(response.body());
            }

            @Override
            public void onFailure(retrofit2.Call<MainProducts> call, Throwable t) {
                iLoadig.errorLoading(t.getMessage());
            }
        });
    }

    private void openPreviewActivity(Product product){
        Intent intent = new Intent(getContext(),ProductPreviewActivity.class);
        intent.putExtra(ProductPreviewActivity.PRODUCT,product);
        startActivityForResult(intent,Actions.START_PREVIEW_ACTIVITY);
    }

    @Override
    public void itemClicked(int blockPosition, int position) {
        selectedBlock = blockPosition;
        selectedProduct = position;
        openPreviewActivity(mMainProducts.getCategories().get(blockPosition).getProducts().get(position));
    }

    private void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Actions.START_PREVIEW_ACTIVITY:
                switch (resultCode){
                    case Actions.RESULT_PREVIEW_TO_FAVORITE:
                        break;
                    case Actions.RESULT_PREVIEW_TO_NON_FAVORITE:
                        break;
                }
                break;
        }
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getActionCode()){
            case EventsActions.ADD_TO_FAVORITE:
                mMainProducts.getCategories().get(selectedBlock).getProducts().get(selectedProduct).setFavorite(true);
                dataManager.getProducts().getCategories().get(selectedBlock).getProducts().get(selectedProduct).setFavorite(true);
                break;
            case EventsActions.REMOVE_FROM_FAVORITE:
                mMainProducts.getCategories().get(selectedBlock).getProducts().get(selectedProduct).setFavorite(false);
                dataManager.getProducts().getCategories().get(selectedBlock).getProducts().get(selectedProduct).setFavorite(false);
                break;
        }
    }
}
