package com.example.nds.primroza.classes;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@JsonAdapter(MainProducts.GetTemplateDeserializer.class)
public class MainProducts implements Parcelable {

    private ArrayList<ProductList> categories;

    public ArrayList<ProductList> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<ProductList> categories) {
        this.categories = categories;
    }


    public static class GetTemplateDeserializer implements JsonDeserializer<MainProducts> {

        @Override
        public MainProducts deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            MainProducts getProducts = new MainProducts();
            getProducts.setCategories(new ArrayList<>());
            JsonElement root = new JsonParser().parse(json.toString());
            JsonObject object = root.getAsJsonObject().getAsJsonObject();
            ArrayList<ProductList> categoties = new ArrayList<>();
            for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
                ArrayList<Product> arrayProdusts =(context.deserialize(entry.getValue(), ArrayList.class));
                ProductList productList = new ProductList();
                ArrayList<Product> parsedProdusts = new ArrayList<>();
                JsonArray jar = new Gson().toJsonTree(arrayProdusts).getAsJsonArray();
                for(int i =0; i < jar.size(); i++){
                   Product product = new Gson().fromJson(jar.get(i), Product.class);
                   parsedProdusts.add(product);
                }
                productList.setProducts(parsedProdusts);
                productList.setName(entry.getKey());
                categoties.add(productList);
            }
            getProducts.setCategories(categoties);
            return getProducts;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.categories);
    }

    public MainProducts() {
    }

    protected MainProducts(Parcel in) {
        this.categories = in.createTypedArrayList(ProductList.CREATOR);
    }

    public static final Creator<MainProducts> CREATOR = new Creator<MainProducts>() {
        @Override
        public MainProducts createFromParcel(Parcel source) {
            return new MainProducts(source);
        }

        @Override
        public MainProducts[] newArray(int size) {
            return new MainProducts[size];
        }
    };
}
/*
@JsonAdapter(GetTemplateEntity.GetTemplateDeserializer.class)
public class GetTemplateEntity extends BaseEntity {

    public static final String ADDITIONAL_DATA = "additionalData";

    @SerializedName("additionalData")
    @Expose
    AdditionalDataEntity additionalData;

    Map<String, String> data;

    public AdditionalDataEntity getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(AdditionalDataEntity additionalData) {
        this.additionalData = additionalData;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public static class GetTemplateDeserializer implements JsonDeserializer<GetTemplateEntity> {

        @Override
        public GetTemplateEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            GetTemplateEntity getTemplate = new GetTemplateEntity();
            JsonElement root = new JsonParser().parse(json.toString());
            JsonObject object = root.getAsJsonObject().getAsJsonObject();
            Map<String, String> map = new HashMap<>();
            for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
                if (!entry.getKey().equalsIgnoreCase(ADDITIONAL_DATA)) {
                    map.put(entry.getKey(), context.deserialize(entry.getValue(), String.class));
                } else {
                    getTemplate.setAdditionalData(context.deserialize(entry.getValue(), AdditionalDataEntity.class));
                }
            }
            getTemplate.setData(map);

            return getTemplate;
        }
    }
}
 */