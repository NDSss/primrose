package com.example.nds.primroza.products.Block;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Urls;
import com.example.nds.primroza.classes.Product;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ProductBlockAdapter extends RecyclerView.Adapter<ProductBlockAdapter.ViewHolder> {

    private ProductBlock.OnProductBlockItemClickedListener onProductBlockItemClickedListener;

    private ArrayList<Product> products;
    private int blockPosition;

    public  ProductBlockAdapter(int blockPosition,ArrayList<Product> products){
        this.blockPosition = blockPosition;
        this.products = products;
    }

    public void setOnProductBlockItemClickedListener(ProductBlock.OnProductBlockItemClickedListener onProductBlockItemClickedListener) {
        this.onProductBlockItemClickedListener = onProductBlockItemClickedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(products.get(position).getTitle());
        holder.tvPrice.setText(products.get(position).getPrice());
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(Urls.FULL_IMAGE+products.get(position).getPhotos().get(0)))
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .setResizeOptions(new ResizeOptions(300,300))
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setControllerListener(new MyControllerListener())
                .build();
        holder.ivPicture.setController(controller);
    }

    public class MyControllerListener extends BaseControllerListener<ImageInfo> {

        public MyControllerListener() {
        }

        @Override
        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
            Log.i("DraweeUpdate", "onFinalImageSet: ");
        }

        @Override
        public void onFailure(String id, Throwable throwable) {
            Log.i("DraweeUpdate", "Image failed to load: " + throwable.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView ivPicture;
        TextView tvName;
        TextView tvPrice;
        TextView tvTime;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivPicture = itemView.findViewById(R.id.iv_item_product);
            tvName = itemView.findViewById(R.id.tv_item_product_name);
            tvPrice = itemView.findViewById(R.id.tv_item_product_price);
            tvTime = itemView.findViewById(R.id.tv_item_product_time);
            itemView.setOnClickListener(v->onProductBlockItemClickedListener.itemClicked(blockPosition,getAdapterPosition()));
        }
    }
}
