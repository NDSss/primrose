package com.example.nds.primroza.map.view;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.nds.primroza.R;
import com.example.nds.primroza.map.bottomsheet.MapBottomSheet;
import com.example.nds.primroza.map.callback.IAtmCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    @BindView(R.id.mv_atm)
    MapView mvAtm;
    @BindView(R.id.view_map_bottom_sheet)
    RelativeLayout rlBottomSheetNew;

    private GoogleMap mGoogleMap;
    private IAtmCallback mAtmCallback;
    private MapBottomSheet.IRemoveView mHideBottomSheet;
    private HashMap<String, Integer> mMarkerList = new HashMap<>();
    private BottomSheetBehavior mBottomSheetBehavior;

    private Unbinder mUnbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mvAtm.onCreate(savedInstanceState);
        mvAtm.onResume();
        mvAtm.getMapAsync(this);

//        if (mGoogleMap != null) {
//            mPresenter.setMarkers();
//        }
        mBottomSheetBehavior = BottomSheetBehavior.from(rlBottomSheetNew);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        rlBottomSheetNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mvAtm.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mvAtm.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mPresenter.setLocation();
        mvAtm.onDestroy();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;

        //init google map preferences
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        if (mAtmCallback != null) {
            mAtmCallback.onAttach(this);
        }
        setAtmData(createMoscowLocation());
    }

    public void setAtmData(Location location) {
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        LatLngBounds bounds = null;
        int padding = (int) (width * 0.30);
        clearMap();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if (location != null) {
            showMyLocation(location);
            builder.include(new LatLng(location.getLatitude(), location.getLongitude()));
            LatLngBounds bounds2 = builder.build();
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds2, width, height, padding));
        }
        Marker currentMarker = null;
        currentMarker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
        builder.include(new LatLng(location.getLatitude(), location.getLongitude()));
        mMarkerList.put(currentMarker.getId(), 0);
        bounds = builder.build();
        mGoogleMap.setOnMarkerClickListener(marker -> {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            return false;
        });
        if (bounds != null) {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
        }

    }

    public void showMyLocation(Location location) {
//        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_atm_my_location)));
    }

    private Location createMoscowLocation() {
        Location location = new Location(LocationManager.GPS_PROVIDER);
        double latitudeMoscow = 55.75343;
        double longitudeMoscow = 37.51556;
        location.setLatitude(latitudeMoscow);
        location.setLongitude(longitudeMoscow);
        return location;
    }

    private void clearMap() {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
        }
        if (mMarkerList != null) {
            mMarkerList.clear();
        }
    }

}
