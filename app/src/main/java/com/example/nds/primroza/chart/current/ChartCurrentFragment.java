package com.example.nds.primroza.chart.current;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.DataManager;
import com.example.nds.primroza.base.IOnAdapterItemSelected;
import com.example.nds.primroza.base.eventbus.IEventBusObserver;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.base.events.EventsActions;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.products.preview.ProductPreviewActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ChartCurrentFragment extends Fragment implements IEventBusObserver,IOnAdapterItemSelected {
    public static final String ITEMS_COUNT = "ChartCurrentFragment.ITEMS_COUNT";
    @BindView(R.id.tv_fragment_chart_current_total_price)
    TextView tvTotalPrice;
    @BindView(R.id.btn_fragment_chart_current_order)
    Button btnOrder;
    @BindView(R.id.rl_fragment_chart_current_confirm_container)
    RelativeLayout rlConfirmContainer;

    private String TAG = "ChartCurrentFragment";
    @BindView(R.id.rv_fragment_chart_current)
    RecyclerView rvCurrent;
    Unbinder unbinder;
    private int envents;
    private ArrayList<Product> products;
    private OrderDetailAdapter adapter;
    private double totalSum =0;
    @Inject
    protected DataManager dataManager;

    public static ChartCurrentFragment newInstance(Bundle bundle) {
        ChartCurrentFragment fragment = new ChartCurrentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        envents = args.getInt(ITEMS_COUNT);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        products = new ArrayList<>();
        App.get(getContext()).getEventBus().addObserver(this::onEvent);
        App.get(getContext()).getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_chart_current, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCurrent.setLayoutManager(layoutManager);
        adapter = new OrderDetailAdapter();
        adapter.setData(dataManager.getChart());
        adapter.setListener(this::onItemSelected);
        rvCurrent.setAdapter(adapter);
        setTotalPrice();
    }
    private void setTotalPrice(){
        totalSum = 0;
        for(Product product:dataManager.getChart()){
            totalSum+=Double.parseDouble(product.getPrice());
        }
        tvTotalPrice.setText("Общая сумма: "+String.valueOf(totalSum));
    }

    @Override
    public void onEvent(Event event) {
        Log.d(TAG, "onEvent: ");
        switch (event.getActionCode()){
            case EventsActions.REMOVE_CART:
                adapter.notifyDataSetChanged();
                setTotalPrice();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemSelected(int position) {
        Intent intent = new Intent(getContext(),ProductPreviewActivity.class);
        intent.putExtra(ProductPreviewActivity.CART_POSITION,position);
        startActivityForResult(intent,Actions.START_PREVIEW_ACTIVITY);
    }
}
