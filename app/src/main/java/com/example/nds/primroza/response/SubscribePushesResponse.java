package com.example.nds.primroza.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SubscribePushesResponse implements Parcelable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    public SubscribePushesResponse() {
    }

    protected SubscribePushesResponse(Parcel in) {
        this.name = in.readString();
    }

    public static final Parcelable.Creator<SubscribePushesResponse> CREATOR = new Parcelable.Creator<SubscribePushesResponse>() {
        @Override
        public SubscribePushesResponse createFromParcel(Parcel source) {
            return new SubscribePushesResponse(source);
        }

        @Override
        public SubscribePushesResponse[] newArray(int size) {
            return new SubscribePushesResponse[size];
        }
    };
}
