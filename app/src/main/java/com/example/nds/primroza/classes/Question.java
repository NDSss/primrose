package com.example.nds.primroza.classes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Question implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("manager_id")
    private int manager_id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("status")
    private int status;
    @SerializedName("title")
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.manager_id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.status);
        dest.writeString(this.title);
    }

    public Question() {
    }

    protected Question(Parcel in) {
        this.id = in.readInt();
        this.manager_id = in.readInt();
        this.user_id = in.readInt();
        this.status = in.readInt();
        this.title = in.readString();
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
