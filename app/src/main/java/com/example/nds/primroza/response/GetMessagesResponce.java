package com.example.nds.primroza.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.nds.primroza.classes.ChatMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.JsonAdapter;

import java.lang.reflect.Type;
import java.util.ArrayList;

@JsonAdapter(GetMessagesResponce.GetMessagesDeserializer.class)
public class GetMessagesResponce implements Parcelable {
    private ArrayList<ChatMessage> messages;

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.messages);
    }

    public GetMessagesResponce() {
    }

    protected GetMessagesResponce(Parcel in) {
        this.messages = in.createTypedArrayList(ChatMessage.CREATOR);
    }

    public static final Parcelable.Creator<GetMessagesResponce> CREATOR = new Parcelable.Creator<GetMessagesResponce>() {
        @Override
        public GetMessagesResponce createFromParcel(Parcel source) {
            return new GetMessagesResponce(source);
        }

        @Override
        public GetMessagesResponce[] newArray(int size) {
            return new GetMessagesResponce[size];
        }
    };

    public static class GetMessagesDeserializer implements JsonDeserializer<GetMessagesResponce>{

        @Override
        public GetMessagesResponce deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            GetMessagesResponce responce = new GetMessagesResponce();
            ArrayList<ChatMessage> messages = new ArrayList<>();
            for(JsonElement el:((JsonArray)json)){
                messages.add(context.deserialize(el,ChatMessage.class));
            }
            responce.setMessages(messages);
            return responce;
        }
    }
}
