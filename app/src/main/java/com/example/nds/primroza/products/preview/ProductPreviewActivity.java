package com.example.nds.primroza.products.preview;

import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.Urls;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.base.events.EventsActions;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.products.Block.ProductBlockAdapter;
import com.example.nds.primroza.products.order.OrderActivity;
import com.example.nds.primroza.products.preview.adapter.PreviewAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProductPreviewActivity extends BaseLoadingActivity {
    private String TAG = "ProductPreviewActivity";
    public static final String PRODUCT = "ProductPreviewActivity.PRODUCT";
    public static final String CART_POSITION = "ProductPreviewActivity.CART_POSITION";
    @BindView(R.id.rv_preview_image)
    RecyclerView rvImages;
    @BindView(R.id.fab_product_buy)
    FloatingActionButton fabProductBuy;
    @BindView(R.id.fab_preview_favorite)
    FloatingActionButton fabProductFavorite;
    @BindView(R.id.tv_preview_price)
    TextView tvPreviewPrice;
    @BindView(R.id.tv_preview_name)
    TextView tvPreviewName;
    @BindView(R.id.tv_preview_exist)
    TextView tvPreviewExist;
    @BindView(R.id.tv_preview_size)
    TextView tvPreviewSize;
    @BindView(R.id.tv_preview_size_width)
    TextView tvPreviewSizeWidth;
    @BindView(R.id.tv_preview_size_height)
    TextView tvPreviewSizeHeight;
    @BindView(R.id.tv_preview_composition)
    TextView tvPreviewComposition;
    @BindView(R.id.tv_preview_description)
    TextView tvPreviewDescription;
    private Product product;
    private PreviewAdapter adapter;
    private int cartPosition;
    private boolean isRemoveMode = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (getIntent().getExtras().containsKey(PRODUCT)) {
            product = (Product) getIntent().getExtras().get(PRODUCT);
        }
        if(getIntent().getExtras().containsKey(CART_POSITION)){
            cartPosition = getIntent().getExtras().getInt(CART_POSITION);
        }
        if (product != null) {
            initView(product, false);
        } else {
            isRemoveMode = true;
            product = dataManager.getChart().get(cartPosition);
            initView(product,true);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_product_preview;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private void initView(Product product, boolean isRemoveMode) {
        Log.d(TAG, "initView: ");
        setTitle(product.getTitle());
        tvPreviewPrice.setText(product.getPrice());
        tvPreviewName.setText(product.getTitle());
        tvPreviewDescription.setText(product.getDescription()==null?"null":product.getDescription());
        LinearLayoutManager llManager = new LinearLayoutManager(this);
        llManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvImages.setLayoutManager(llManager);
        adapter = new PreviewAdapter(product.getPhotos());
        rvImages.setAdapter(adapter);
        if(isRemoveMode){
            fabProductBuy.setOnClickListener(v->removeFromChart());
            fabProductBuy.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove_cart));
        } else {
            fabProductBuy.setOnClickListener(v -> openOrderActivity());
        }
        if(product.isFavorite()){
            fabProductFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_filled));
            fabProductFavorite.setOnClickListener(v -> removeFromFavorite());
        }else {
            fabProductFavorite.setOnClickListener(v -> setFaforite());
        }
    }

    private void setFaforite(){
        Log.d(TAG, "setFaforite: ");
//        App.get(this).getEventBus().notifyEvent(new Event(11));
//        dataManager.addToChart(product);
        fabProductFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_filled));
        App.get(this).getEventBus().notifyEvent(new Event(EventsActions.ADD_TO_FAVORITE));
        fabProductFavorite.setOnClickListener(v->removeFromFavorite());
    }

    private void removeFromFavorite(){
        fabProductFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite));
        App.get(this).getEventBus().notifyEvent(new Event(EventsActions.REMOVE_FROM_FAVORITE));
        fabProductFavorite.setOnClickListener(v->setFaforite());
    }

    private void removeFromChart(){
        dataManager.getChart().remove(product);
        App.get(this).getEventBus().notifyEvent(new Event(EventsActions.REMOVE_CART));
        finish();
    }

    private void openOrderActivity(){
        Intent intent = new Intent(this,OrderActivity.class);
        intent.putExtra(OrderActivity.PRODUCT,product);
        startActivityForResult(intent,1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        } if(resultCode == Actions.RESULT_PREVIEW_TO_FAVORITE){
            finish();
        }
    }

    @Override
    public void onEvent(Event event) {

    }
}
