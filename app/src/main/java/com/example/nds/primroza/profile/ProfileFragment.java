package com.example.nds.primroza.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.DataManager;
import com.example.nds.primroza.classes.City;
import com.example.nds.primroza.classes.Profile;
import com.example.nds.primroza.loginregistration.LoginActivity;
import com.example.nds.primroza.main.IWorkWithShared;
import com.example.nds.primroza.support.SupportQuestionsActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProfileFragment extends Fragment implements ProfileAdapter.IProfileMenuClicked {

    @BindView(R.id.ll_profile_authorized_content)
    LinearLayout llProfileAuthorizedContent;
    @BindView(R.id.ll_profile_unauthorized_content)
    LinearLayout llProfileUnauthorizedContent;
    @BindView(R.id.tv_profile_name)
    TextView tvName;
    @BindView(R.id.tv_profile_phone)
    TextView tvPhone;
    @BindView(R.id.tv_profile_city)
    TextView tvCity;
    @BindView(R.id.tv_profile_logout)
    TextView tvLogout;
    @BindView(R.id.rv_fragment_profile)
    RecyclerView rvMenu;
    Unbinder unbinder;
    private City city;
    private ProfileAdapter adapter;
    @Inject
    DataManager mDatamanager;

    private IWorkWithShared shared;

    public void setShared(IWorkWithShared shared) {
        this.shared = shared;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inject();
        initView();
        setListeners();
    }

    private void inject(){
        App.get(getContext()).getActivityComponent().inject(this);
    }

    private void initView(){
        if(mDatamanager.getLogin()!=null){
            llProfileAuthorizedContent.setVisibility(View.VISIBLE);
            llProfileUnauthorizedContent.setVisibility(View.GONE);
            tvLogout.setVisibility(View.VISIBLE);
            tvName.setText(mDatamanager.getLogin().getEmail());
//            tvCity.setText(profile.getCityName());
//            tvPhone.setText(profile.getPhone());
        } else {
            llProfileAuthorizedContent.setVisibility(View.GONE);
            llProfileUnauthorizedContent.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.GONE);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMenu.setLayoutManager(layoutManager);
        adapter = new ProfileAdapter();
        adapter.setListener(this::menuClick);
        rvMenu.setAdapter(adapter);
    }

    private void setListeners(){
        llProfileUnauthorizedContent.setOnClickListener(v->openLoginActivity());
        tvLogout.setOnClickListener(v->logOut());
    }

    private void openLoginActivity(){
        Intent intent = new Intent(getActivity(),LoginActivity.class);
        startActivityForResult(intent,Actions.START_LOGIN_ACTIVITY);
    }

    private void logOut(){
        Profile profile = new Profile();
        profile.setLogin(false);
        shared.setLoginStatus(profile);
        initView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Actions.START_LOGIN_ACTIVITY:
                switch (resultCode){
                    case Actions.RESULT_LOGIN_OK:
//                        shared.setLoginStatus(data.getParcelableExtra(LoginActivity.PROFILE));
                        initView();
                        break;
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void menuClick(int menuItem) {
        startActivity(new Intent(getContext(),SupportQuestionsActivity.class));
    }
}
