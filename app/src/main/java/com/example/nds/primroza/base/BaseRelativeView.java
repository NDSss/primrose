package com.example.nds.primroza.base;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public abstract class BaseRelativeView extends RelativeLayout {

    public BaseRelativeView(Context context) {
        super(context);
    }

    public BaseRelativeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseRelativeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
