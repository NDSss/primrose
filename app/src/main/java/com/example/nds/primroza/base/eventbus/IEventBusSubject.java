package com.example.nds.primroza.base.eventbus;

import com.example.nds.primroza.base.events.Event;

public interface IEventBusSubject {
    void addObserver(IEventBusObserver observer);
    void removeObserver(IEventBusObserver observer);
    void notifyEvent(Event event);
}
