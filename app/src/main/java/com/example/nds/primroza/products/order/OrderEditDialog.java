package com.example.nds.primroza.products.order;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.nds.primroza.R;
import com.example.nds.primroza.base.BaseEditDialog;

public class OrderEditDialog extends BaseEditDialog {
    public static final String HINT = "OrderEditDialog.HINT";
    public static final String DATA = "OrderEditDialog.DATA";
    public static OrderEditDialog newInstance(String hint, String data){
        OrderEditDialog orderEditDialog = new OrderEditDialog();
        Bundle bundle = new Bundle();
        bundle.putString(HINT,hint);
        bundle.putString(DATA,data);
        orderEditDialog.setArguments(bundle);
        return orderEditDialog;
    }

    private String hint;
    private String data;
    private TextInputLayout til;
    private Button btnSave;
    private TextInputEditText tiet;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs(){
        if(getArguments()!=null){
            hint = getArguments().getString(HINT);
            data = getArguments().getString(DATA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_edit,container,true);
        til = view.findViewById(R.id.til_dialog_order_edit);
        btnSave = view.findViewById(R.id.btn_dialog_order_edit);
        btnSave.setOnClickListener(v->save());
        tiet = new TextInputEditText(getContext());
        tiet.setHint(hint);
        tiet.setText(data);
        til.addView(tiet);
        return view;
    }

    private void save(){
        edit(tiet.getText().toString());
        dismiss();
    }
}
