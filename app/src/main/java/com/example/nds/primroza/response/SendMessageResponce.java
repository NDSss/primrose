package com.example.nds.primroza.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SendMessageResponce implements Parcelable {
    @SerializedName("question_id")
    private int question_id;

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.question_id);
    }

    public SendMessageResponce() {
    }

    protected SendMessageResponce(Parcel in) {
        this.question_id = in.readInt();
    }

    public static final Parcelable.Creator<SendMessageResponce> CREATOR = new Parcelable.Creator<SendMessageResponce>() {
        @Override
        public SendMessageResponce createFromParcel(Parcel source) {
            return new SendMessageResponce(source);
        }

        @Override
        public SendMessageResponce[] newArray(int size) {
            return new SendMessageResponce[size];
        }
    };
}
