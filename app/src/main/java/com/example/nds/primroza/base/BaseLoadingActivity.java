package com.example.nds.primroza.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.DataManager;
import com.example.nds.primroza.base.eventbus.IEventBusObserver;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.response.LoginResponse;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseLoadingActivity extends AppCompatActivity implements IBaseLoadingActivity, IEventBusObserver {
    private static final String PROGRESS_TAG = "BaseLoadingActivity.PROGRESS_TAG";
    ViewProgressView progressView;
    View mainView;
    @Inject protected DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        progressView = findViewById(R.id.progress_view);
        ButterKnife.bind(this);
        App.get(this).getEventBus().addObserver(this);
        inject(App.get(this).getActivityComponent());
    }

    protected abstract void inject(ActivityComponent activityComponent);

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.get(this).getEventBus().removeObserver(this);
    }

    protected abstract int getLayout();

    public void startLoading(){
        progressView.startLoading();
    }
    public void completeLoading(){
        progressView.completeLoading();
    }
    public void errorLoading(String message){
        progressView.completeLoading();
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

}
