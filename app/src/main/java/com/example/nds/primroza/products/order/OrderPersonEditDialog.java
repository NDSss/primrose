package com.example.nds.primroza.products.order;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.base.BaseEditDialog;
import com.github.pinball83.maskededittext.MaskedEditText;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.PolyMaskTextChangedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OrderPersonEditDialog extends BaseEditDialog {
    public static final String NAME = "OrderPersonEditDialog.NAME";
    public static final String PHONE = "OrderPersonEditDialog.PHONE";

    @BindView(R.id.sw_dialog_order_person_edit)
    Switch swDialogOrderPersonEdit;
    @BindView(R.id.tv_dialog_order_person_edit_me)
    TextView tvDialogOrderPersonEditMe;
    @BindView(R.id.rl_dialog_order_person_edit_switch_container)
    RelativeLayout rlDialogOrderPersonEditSwitchContainer;
    @BindView(R.id.et_dialog_order_person_edit_name)
    EditText etDialogOrderPersonEditName;
    @BindView(R.id.met_dialog_order_person_edit_phone)
    EditText metDialogOrderPersonEditPhone;
    @BindView(R.id.til_dialog_order_person_edit_phone)
    TextInputLayout tilDialogOrderPersonEditPhone;
    @BindView(R.id.btn_dialog_order_person_edit)
    Button btnSave;
    Unbinder unbinder;
    private String name;
    private String phone;
    public final List<String> affineFormats = new ArrayList<>();

    public static OrderPersonEditDialog newInstance(String name,String phone){
        OrderPersonEditDialog dialog = new OrderPersonEditDialog();
        Bundle bundle = new Bundle();
        bundle.putString(NAME,name);
        bundle.putString(PHONE,phone);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
        affineFormats.add("+7([000])[000]-[00]-[00]");
    }

    private void getArgs(){
        if(getArguments()!=null){
            name = getArguments().getString(NAME);
            phone = getArguments().getString(PHONE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_person_edit, container, false);
        unbinder = ButterKnife.bind(this, view);
        swDialogOrderPersonEdit.setOnCheckedChangeListener((c,b)->checked(b));
        btnSave.setOnClickListener(v->save());
        MaskedTextChangedListener mPhoneListener = new PolyMaskTextChangedListener(
                "+7([000])[000]-[00]-[00]",
                affineFormats,
                true,
                metDialogOrderPersonEditPhone,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                    }
                }
        );
        metDialogOrderPersonEditPhone.addTextChangedListener(mPhoneListener);
        return view;
    }

    private void checked(boolean isMe){
        if(!isMe){
            etDialogOrderPersonEditName.setText(name);
            metDialogOrderPersonEditPhone.setText(phone);
            metDialogOrderPersonEditPhone.setEnabled(false);
            etDialogOrderPersonEditName.setEnabled(false);
        } else {
            etDialogOrderPersonEditName.setText(null);
            metDialogOrderPersonEditPhone.setText(null);
            metDialogOrderPersonEditPhone.setEnabled(true);
            etDialogOrderPersonEditName.setEnabled(true);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etDialogOrderPersonEditName.setText(name);
        metDialogOrderPersonEditPhone.setText(phone);
        etDialogOrderPersonEditName.setEnabled(false);
        metDialogOrderPersonEditPhone.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void save(){
        edit(etDialogOrderPersonEditName.getText().toString()+", "+metDialogOrderPersonEditPhone.getText().toString());
        dismiss();
    }
}
