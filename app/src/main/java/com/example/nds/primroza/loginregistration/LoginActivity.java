package com.example.nds.primroza.loginregistration;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nds.primroza.BuildConfig;
import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.Utils;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.classes.MainProducts;
import com.example.nds.primroza.classes.Profile;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.response.LoginResponse;
import com.example.nds.primroza.response.SubscribePushesResponse;
import com.example.nds.primroza.servises.IProductServise;
import com.github.pinball83.maskededittext.MaskedEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseLoadingActivity {
    public static final String TAG = "LoginActivity";
    public static final String PROFILE = "LoginActivity.PROFILE";
    @BindView(R.id.et_login_phone)
    MaskedEditText etLoginPhone;
    @BindView(R.id.et_login_password)
    EditText etLoginPassword;
    @BindView(R.id.tv_login_registration)
    TextView tvLoginRegistration;
    @BindView(R.id.btn_login_login)
    Button btnLoginLogin;
    @BindView(R.id.til_login_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_login_phone)
    TextInputLayout tilPhone;
    @BindView(R.id.et_activity_login_email)
    EditText etEmail;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListeners();
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    private void setListeners(){
        btnLoginLogin.setOnClickListener(v->login());
        tvLoginRegistration.setOnClickListener(v->openSignInScreen());
        List<Observable<Boolean>> observers = new ArrayList<>();
        observers.add(Utils.notEmptyObserver(etLoginPassword,2,tilPassword));
        observers.add(Utils.getPhoneWatcher(etLoginPhone,tilPhone));
//        Disposable validationDisposable = Observable.combineLatest(observers,objects -> {
//            boolean state = true;
//            for(Object object: objects){
//                state &= (Boolean) object;
//            }
//            return state;
//        }).subscribe(s->btnLoginLogin.setEnabled(s));
//        mCompositeDisposable.add(validationDisposable);
    }

    private void login(){
//        if(etLoginPhone.getText().toString().equalsIgnoreCase(getResources().getString(R.string.phone_number))){
//            Intent intent = new Intent();
//            intent.putExtra(PROFILE,getTestProfile());
//            setResult(Actions.RESULT_LOGIN_OK,intent);
//            finish();
//        }
        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<LoginResponse> weatherResponceCall = productServise.login(etEmail.getText().toString(),etLoginPassword.getText().toString());
        weatherResponceCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(retrofit2.Call<LoginResponse> call, Response<LoginResponse> response) {
                handleLogin(response.body());
            }

            @Override
            public void onFailure(retrofit2.Call<LoginResponse> call, Throwable t) {
                errorLoading(t.getMessage());
            }
        });
    }

    private void handleLogin(LoginResponse response){
        dataManager.setLogin(response);
        getToken();
    }

    private void getToken(){

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(!task.isSuccessful()){
                    Log.d(TAG, "onComplete: token failed");
                    return;
                }
                subcribe(task.getResult().getToken());
                Log.d(TAG, "onComplete: token : "+ task.getResult().getToken());
            }
        });
    }

    private void subcribe(String token){
        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<Void> weatherResponceCall = productServise.subscribePushes(dataManager.getLogin().getApiKey(),BuildConfig.VERSION_NAME,token);
        weatherResponceCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(retrofit2.Call<Void> call, Response<Void> response) {
                handleSub();
            }

            @Override
            public void onFailure(retrofit2.Call<Void> call, Throwable t) {
                handleError(t.getMessage());
            }
        });
    }

    private void handleError(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    private void handleSub(){
            setResult(Actions.RESULT_LOGIN_OK);
            finish();
    }

    private Profile getTestProfile(){
        Profile profile = new Profile();
        profile.setPhone(getResources().getString(R.string.phone_number));
        profile.setName("NDS");
        profile.setCityName("Москва");
        profile.setMail("NDS@mail.ru");
        profile.setLogin(true);
        return profile;
    }
    private void openSignInScreen(){
        Intent intent = new Intent(this,SignInActivity.class);
        startActivityForResult(intent,Actions.START_SIGN_IN_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case Actions.START_SIGN_IN_ACTIVITY:
                switch (resultCode){
                    case Actions.RESULT_SIGN_IN_SIGNED:
                        Intent intent = new Intent();
                        intent.putExtra(PROFILE,(Profile)data.getParcelableExtra(SignInActivity.PROFILE));
                        setResult(Actions.RESULT_LOGIN_OK,intent);
                        finish();
                        break;
                }
                break;
        }
    }

    @Override
    public void onEvent(Event event) {

    }
}
