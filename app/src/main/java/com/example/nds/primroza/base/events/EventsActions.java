package com.example.nds.primroza.base.events;

public class EventsActions {
    public static final int ADD_TO_CART = 1;
    public static final int ADD_TO_FAVORITE = 2;
    public static final int REMOVE_FROM_FAVORITE = 3;
    public static final int REMOVE_CART = 4;
    public static final int PUSH_RECIVED = 5;
}
