package com.example.nds.primroza.products.preview.adapter;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Urls;
import com.example.nds.primroza.products.Block.ProductBlockAdapter;
import com.example.nds.primroza.products.preview.ProductPreviewActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;

import retrofit2.http.Url;

public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewHolder> {

    private ArrayList<String> images;

    public PreviewAdapter(ArrayList<String> images){
        this.images = images;
    }

    public void setData(ArrayList<String> images){
        this.images = images;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ImageRequest request = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(Urls.FULL_IMAGE+images.get(position)))
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .setResizeOptions(new ResizeOptions(300,300))
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setControllerListener(new PreviewAdapter.MyControllerListener())
                .build();
        holder.sdvPreview.setController(controller);
    }
    public class MyControllerListener extends BaseControllerListener<ImageInfo> {

        public MyControllerListener() {
        }

        @Override
        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
            Log.i("DraweeUpdate", "onFinalImageSet: ");
        }

        @Override
        public void onFailure(String id, Throwable throwable) {
            Log.i("DraweeUpdate", "Image failed to load: " + throwable.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView sdvPreview;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sdvPreview = itemView.findViewById(R.id.spd_item_preview);
        }
    }
}
