package com.example.nds.primroza.profile;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nds.primroza.R;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private IProfileMenuClicked listener;

    public void setListener(IProfileMenuClicked listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (position){
            case 0:
                holder.info.setText(holder.info.getContext().getResources().getStringArray(R.array.profile_menu)[position]);
                holder.desc.setText("");
                holder.ic.setImageDrawable(holder.ic.getContext().getResources().getDrawable(R.drawable.ic_invite_friends));
                break;
            case 1:
                holder.info.setText(holder.info.getContext().getResources().getStringArray(R.array.profile_menu)[position]);
                holder.desc.setText("");
                holder.ic.setImageDrawable(holder.ic.getContext().getResources().getDrawable(R.drawable.ic_promo));
                break;
            case 2:
                holder.info.setText(holder.info.getContext().getResources().getStringArray(R.array.profile_menu)[position]);
                holder.desc.setText("");
                holder.ic.setImageDrawable(holder.ic.getContext().getResources().getDrawable(R.drawable.ic_favorite));
                break;
            case 3:
                holder.info.setText(holder.info.getContext().getResources().getStringArray(R.array.profile_menu)[position]);
                holder.desc.setText("");
                holder.ic.setImageDrawable(holder.ic.getContext().getResources().getDrawable(R.drawable.ic_bank_card));
                break;
            case 4:
                holder.info.setText(holder.info.getContext().getResources().getStringArray(R.array.profile_menu)[position]);
                holder.desc.setText("");
                holder.ic.setImageDrawable(holder.ic.getContext().getResources().getDrawable(R.drawable.ic_help));
                holder.devider.setVisibility(View.GONE);
                holder.info.setOnClickListener(v->listener.menuClick(4));
                holder.ic.setOnClickListener(v->listener.menuClick(4));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView info;
        public TextView desc;
        public ImageView ic;
        public View devider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            info = itemView.findViewById(R.id.tv_item_profile_info);
            desc = itemView.findViewById(R.id.tv_item_profile_desc);
            ic = itemView.findViewById(R.id.iv_item_profile);
            devider = itemView.findViewById(R.id.v_item_profile_devider);
        }
    }

    public interface IProfileMenuClicked{
        void menuClick(int menuItem);
    }
}
