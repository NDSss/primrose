package com.example.nds.primroza.support;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.base.events.EventsActions;
import com.example.nds.primroza.classes.ChatMessage;
import com.example.nds.primroza.classes.Question;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.response.GetMessagesResponce;
import com.example.nds.primroza.response.GetQuestionsResponse;
import com.example.nds.primroza.response.SendMessageResponce;
import com.example.nds.primroza.servises.IProductServise;

import butterknife.BindView;
import retrofit2.Callback;
import retrofit2.Response;

public class supportChatActivity extends BaseLoadingActivity {
    public static final String QUESTION = "supportChatActivity.QUESTION";
    public static final String TAG = "supportChatActivity";
    @BindView(R.id.rv_activity_support_chat)
    RecyclerView rvChat;
    @BindView(R.id.et_activity_chat_input)
    EditText etMessage;
    @BindView(R.id.btn_activity_chat_input_send_message)
    Button btnSend;
    private Question question;
    private ChatAdapter adapter;
    private ChatMessage message;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Как купить цветы в приложении?");
        question = getIntent().getExtras().getParcelable(QUESTION);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvChat.setLayoutManager(manager);
        adapter = new ChatAdapter(dataManager.getLogin().getId());
        rvChat.setAdapter(adapter);
        btnSend.setOnClickListener(v->sendMessage());
        getMessages();
    }

    private void getMessages(){
        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<GetMessagesResponce> weatherResponceCall = productServise.getMessages(question.getId());
        weatherResponceCall.enqueue(new Callback<GetMessagesResponce>() {
            @Override
            public void onResponse(retrofit2.Call<GetMessagesResponce> call, Response<GetMessagesResponce> response) {
                handleGetMessages(response.body());
//                completeLoading();
            }

            @Override
            public void onFailure(retrofit2.Call<GetMessagesResponce> call, Throwable t) {
                handleError(t.getMessage());
            }
        });
    }

    private void sendMessage(){
        message = new ChatMessage();
        message.setMessage(etMessage.getText().toString());
        message.setSender(dataManager.getLogin().getId());

        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<SendMessageResponce> weatherResponceCall = productServise.sendMessage(question.getId(),message.getMessage(),String.valueOf(dataManager.getLogin().getId()));
        weatherResponceCall.enqueue(new Callback<SendMessageResponce>() {
            @Override
            public void onResponse(retrofit2.Call<SendMessageResponce> call, Response<SendMessageResponce> response) {
                handleSendMessage(response.body());
//                completeLoading();
            }

            @Override
            public void onFailure(retrofit2.Call<SendMessageResponce> call, Throwable t) {
                handleError(t.getMessage());
            }
        });
    }

    private void handleSendMessage(SendMessageResponce responce){
        adapter.addMessage(message);
        rvChat.scrollToPosition(adapter.getItemCount());
    }

    private void handleGetMessages(GetMessagesResponce body) {
        adapter.setMessages(body.getMessages());
    }

    private void handleError(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_chat;
    }

    @Override
    public void onEvent(Event event) {
        Log.d(TAG, "onEvent: "+String.valueOf(event.getActionCode()));
        if(event.getActionCode()==EventsActions.PUSH_RECIVED){
            getMessages();
        }
    }
}
