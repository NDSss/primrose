package com.example.nds.primroza.app;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.nds.primroza.R;

import java.util.regex.Pattern;

import io.reactivex.subjects.PublishSubject;

public class Utils {

    public static io.reactivex.Observable<Boolean> notEmptyObserver(final EditText editText,TextInputLayout til){
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                subject.onNext(s.length()>0);
                til.setErrorEnabled(!(s.length()>0));
            }
        });
        return subject;
    }
    public static io.reactivex.Observable<Boolean> notEmptyObserver(final EditText editText,int lenth,TextInputLayout til){
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>lenth){
                    subject.onNext(true);
                    til.setErrorEnabled(false);
                } else {
                    til.setErrorEnabled(true);
                    subject.onNext(false);
                    til.setError(String.format(editText.getContext().getResources().getText(R.string.lenth_error).toString(),lenth));
                };
            }
        });
        return subject;
    }


    public static io.reactivex.Observable<Boolean> patternMatcherObserver(final EditText editText, Pattern pattern,TextInputLayout til){
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(pattern.matcher(s.toString()).matches()){
                    subject.onNext(true);
                    til.setErrorEnabled(false);
                } else {
                    til.setErrorEnabled(true);
                    subject.onNext(false);
                    til.setError(editText.getContext().getResources().getText(R.string.email_error));
                }

            }
        });
        return subject;
    }


    public static io.reactivex.Observable<Boolean> getPhoneWatcher(final EditText editText,TextInputLayout til){
        final PublishSubject<Boolean> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==16){
                    subject.onNext(true);
                    til.setErrorEnabled(false);
                } else {
                    til.setErrorEnabled(true);
                    subject.onNext(false);
                    til.setError(editText.getContext().getResources().getText(R.string.phone_error));
                }

            }
        });
        return subject;
    }
}
