package com.example.nds.primroza.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LoginResponse implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("apiKey")
    private String apiKey;
    @SerializedName("email")
    private String email;
    @SerializedName("username")
    private String username;
    @SerializedName("usernameLast")
    private String usernameLast;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameLast() {
        return usernameLast;
    }

    public void setUsernameLast(String usernameLast) {
        this.usernameLast = usernameLast;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.apiKey);
        dest.writeString(this.email);
        dest.writeString(this.username);
        dest.writeString(this.usernameLast);
    }

    public LoginResponse() {
    }

    protected LoginResponse(Parcel in) {
        this.id = in.readInt();
        this.apiKey = in.readString();
        this.email = in.readString();
        this.username = in.readString();
        this.usernameLast = in.readString();
    }

    public static final Parcelable.Creator<LoginResponse> CREATOR = new Parcelable.Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel source) {
            return new LoginResponse(source);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };
}
