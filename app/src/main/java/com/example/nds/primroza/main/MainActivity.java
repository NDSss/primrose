package com.example.nds.primroza.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.base.events.EventsActions;
import com.example.nds.primroza.chart.ChartFragment;
import com.example.nds.primroza.chart.current.ChartCurrentFragment;
import com.example.nds.primroza.classes.City;
import com.example.nds.primroza.classes.Order;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.classes.Profile;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.map.view.MapFragment;
import com.example.nds.primroza.products.ProductFragment;
import com.example.nds.primroza.profile.ProfileFragment;

import java.util.ArrayList;

import butterknife.BindView;


public class MainActivity extends BaseLoadingActivity implements IWorkWithShared {
    private String TAG = "MainActivity";
    public static final String NAME = "MainActivity.NAME";
    public static final String PHONE = "MainActivity.PHONE";
    public static final String MAIL = "MainActivity.MAIL";
    public static final String CITY = "MainActivity.CITY";
    public static final String CITY_PARCEABLE = "MainActivity.CITY_PARCEABLE";
    public static final String ISLOGIN = "MainActivity.ISLOGIN";
    public static final String PRODUCT_FRAGMENT_TAG = "MainActivity.ProductFragment";
    public static final String PROFILE_FRAGMENT_TAG = "MainActivity.ProfileFragment";
    public static final String MAP_FRAGMENT_TAG = "MainActivity.MapFragment";


    private RelativeLayout rlContainer;
    private SharedPreferences sPref;
    ProductFragment productFragment;
    ProfileFragment profileFragment;
    MapFragment mapFragment;
    ChartFragment chartFragment;
    BottomNavigationView navigation;
    private City city;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_products:
                    openFragment(productFragment);
                    return true;
                case R.id.navigation_map:
                    openFragment(mapFragment);
                    return true;
                case R.id.navigation_chart:
                    openFragment(chartFragment);
                    return true;
                case R.id.navigation_profile:
                    openFragment(profileFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    private void openFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().addToBackStack(fragment.getTag()).replace(R.id.rl_main_fragments_container,fragment).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        getArgs();
        sPref = getPreferences(MODE_PRIVATE);
        setCity();
        rlContainer = findViewById(R.id.rl_main_fragments_container);
        createFragments();
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_products);
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private void getArgs(){
        city = dataManager.getProfile().getCity();
    }

    private void createFragments(){
        Log.d(TAG, "createFragments: ");
        productFragment = new ProductFragment();
        productFragment.setCity(city);
        productFragment.setiLoadig(this);
        profileFragment = new ProfileFragment();
        profileFragment.setShared(this);
        mapFragment = new MapFragment();
        chartFragment = ChartFragment.newInstance(0);
//        getSupportFragmentManager().beginTransaction().add(R.id.rl_main_fragments_container,mapFragment)
//                .add(R.id.rl_main_fragments_container,profileFragment)
//                .add(R.id.rl_main_fragments_container,productFragment).commit();
    }

    private void setNotification(int i){
        BottomNavigationHelper.setNotification(getBaseContext(),navigation,R.id.navigation_chart,i);
    }

    @Override
    public Profile isLoggined() {
        Profile profile = new Profile();
        profile.setName(sPref.getString(NAME,"def"));
        profile.setPhone(sPref.getString(PHONE,"def"));
        profile.setMail(sPref.getString(MAIL,"def"));
        profile.setCityName(sPref.getString(CITY,"def"));
        profile.setLogin(sPref.getBoolean(ISLOGIN,false));
        return profile;
    }

    @Override
    public void setLoginStatus(Profile profile){
        SharedPreferences.Editor ed = sPref.edit();
        ed.putBoolean(ISLOGIN, profile.isLogin());
        ed.putString(NAME,profile.getName());
        ed.putString(PHONE,profile.getPhone());
        ed.putString(CITY,profile.getCityName());
        ed.putString(MAIL,profile.getMail());
        ed.commit();
    }

    private void setCity(){
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(CITY,city.getName());
        ed.commit();
    }

    @Override
    public void onEvent(Event event) {
        if(event.getActionCode()==EventsActions.ADD_TO_CART){
            setNotification(dataManager.getChart().size());
        }
        Log.d(TAG, "onEvent: ");
    }
}
