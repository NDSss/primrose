package com.example.nds.primroza.app;

import com.example.nds.primroza.classes.MainProducts;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.classes.Profile;
import com.example.nds.primroza.response.LoginResponse;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataManager {

    private Profile profile;
    private ArrayList<Product> chart;
    private MainProducts products;
    private LoginResponse login;

    @Inject
    DataManager(){
        profile = new Profile();
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ArrayList<Product> getChart() {
        return chart;
    }

    public void setChart(ArrayList<Product> chart) {
        this.chart = chart;
    }

    public void addToChart(Product product){
        if(chart == null){
            chart = new ArrayList<>();
        }
        chart.add(product);
    }

    public MainProducts getProducts() {
        return products;
    }

    public void setProducts(MainProducts products) {
        this.products = products;
    }


    public LoginResponse getLogin() {
        return login;
    }

    public void setLogin(LoginResponse login) {
        this.login = login;
    }
}
