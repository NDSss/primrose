package com.example.nds.primroza.servises;

import com.example.nds.primroza.app.Urls;
import com.example.nds.primroza.classes.CityResponce;
import com.example.nds.primroza.classes.MainProducts;
import com.example.nds.primroza.response.CreateQuestionResponce;
import com.example.nds.primroza.response.GetMessagesResponce;
import com.example.nds.primroza.response.GetQuestionsResponse;
import com.example.nds.primroza.response.LoginResponse;
import com.example.nds.primroza.response.SendMessageResponce;
import com.example.nds.primroza.response.SubscribePushesResponse;

import java.util.LinkedHashMap;

import javax.annotation.PostConstruct;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IProductServise {
    @FormUrlEncoded
    @POST(Urls.product)
    Call<MainProducts> getProducts(@FieldMap(encoded = true) LinkedHashMap<String, Integer> params);

    @POST(Urls.city)
    Call<CityResponce> getCities();

    @FormUrlEncoded
    @POST(Urls.LOGIN)
    Call<LoginResponse> login(@Field("email")String email, @Field("password") String password);

    @FormUrlEncoded
    @POST(Urls.CREATE_QUESTION)
    Call<CreateQuestionResponce> createQuestion(@Field("manager_id")int manager_id,@Field("user_id")int user_id ,@Field("title")String title);

    @FormUrlEncoded
    @POST(Urls.GET_QUESTIONS)
    Call<GetQuestionsResponse> getQuestions(@Field("apiKey")String apiKey);

    @FormUrlEncoded
    @POST(Urls.GET_MESSAGES)
    Call<GetMessagesResponce> getMessages(@Field("question_id")int question_id );

    @FormUrlEncoded
    @POST(Urls.SEND_MESSAGE)
    Call<SendMessageResponce> sendMessage(@Field("question_id")int question_id, @Field("message") String message, @Field("sender") String sender);

    @FormUrlEncoded
    @POST(Urls.SUBSCRIBE_PUSHES)
    Call<Void> subscribePushes(@Field("apiKey")String apiKey, @Field("device_id") String device_id , @Field("token") String token);
}
