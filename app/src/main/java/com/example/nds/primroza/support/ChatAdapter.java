package com.example.nds.primroza.support;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.classes.ChatMessage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_MY = 0;
    private static final int TYPE_THEIR = 1;

    private int myId;
    private ArrayList<ChatMessage> messages;

    public int getSizee(){
        return messages==null?0:messages.size();
    }

    public ChatAdapter(int myId) {
        this.myId = myId;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
        notifyDataSetChanged();

    }

    public void addMessage(ChatMessage message){
        messages.add(message);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position).getSender() == myId) {
            return TYPE_MY;
        } else {
            return TYPE_THEIR;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_MY:
                return new ViewHolderMy(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_my, parent, false));
            case TYPE_THEIR:
                return new ViewHolderTheir(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_their, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case TYPE_MY:
                ((ViewHolderMy)holder).tvMessage.setText(messages.get(position).getMessage());
                break;
            case TYPE_THEIR:
                ((ViewHolderTheir)holder).tvMessage.setText(messages.get(position).getMessage());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return messages==null?0:messages.size();
    }

    public class ViewHolderMy extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_chat_my_message)
        TextView tvMessage;
        public ViewHolderMy(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolderTheir extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_chat_my_message)
        TextView tvMessage;
        public ViewHolderTheir(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
