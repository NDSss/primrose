package com.example.nds.primroza.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.classes.City;
import com.example.nds.primroza.classes.CityResponce;
import com.example.nds.primroza.classes.MainProducts;
import com.example.nds.primroza.classes.Profile;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.di.DaggerActivityComponent;
import com.example.nds.primroza.main.MainActivity;
import com.example.nds.primroza.servises.IProductServise;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseLoadingActivity {

    private static final String TAG = "SplashActivity";

    @BindView(R.id.sp_splash)
    Spinner spCities;
    @BindView(R.id.btn_splash_choose)
    Button btnChoose;
    @BindView(R.id.et_token)
    EditText etToken;
    private ArrayList<City> cities;
    private String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getToken();
        setListeners();
        getCities();
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private void getToken(){

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(!task.isSuccessful()){
                    Log.d(TAG, "onComplete: token failed");
                    return;
                }
                token = task.getResult().getToken();
                etToken.setText(token);
                Log.d(TAG, "onComplete: token : "+ token);
            }
        });
    }

    private void setListeners(){
        btnChoose.setOnClickListener(v->choose());
    }

    private void getCities() {
        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<CityResponce> cityResponceCall = productServise.getCities();
        startLoading();
        cityResponceCall.enqueue(new Callback<CityResponce>() {
            @Override
            public void onResponse(retrofit2.Call<CityResponce> call, Response<CityResponce> response) {
                handleCities(response.body());
                completeLoading();
            }

            @Override
            public void onFailure(retrofit2.Call<CityResponce> call, Throwable t) {
                errorLoading(t.getMessage());
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }

    private void handleCities(CityResponce responce){
        cities = responce.getCities();
        spCities.setAdapter(new CitiesSpinnerAdapter(this,R.layout.item_spinner_cities,cities));
    }

    private void choose(){
        Profile emptyProfile = new Profile();
        emptyProfile.setCity(cities.get(spCities.getSelectedItemPosition()));
        dataManager.setProfile(emptyProfile);
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(MainActivity.CITY_PARCEABLE,cities.get(spCities.getSelectedItemPosition()));
        startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void onEvent(Event event) {

    }
}
