package com.example.nds.primroza.base;

import android.support.v4.app.DialogFragment;

public abstract class BaseEditDialog extends DialogFragment{
    private IBaseEditDialog editDialog;

    protected void edit(String data){
        editDialog.edit(data);
    }

    public void setEditDialog(IBaseEditDialog editDialog) {
        this.editDialog = editDialog;
    }
}
