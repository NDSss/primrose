package com.example.nds.primroza.classes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ChatMessage implements Parcelable {
    @SerializedName("id")
    private long id;
    @SerializedName("time")
    private long time;
    @SerializedName("question_id")
    private int question_id;
    @SerializedName("sender")
    private int sender;
    @SerializedName("view")
    private int view;
    @SerializedName("message")
    private String message;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.time);
        dest.writeInt(this.question_id);
        dest.writeInt(this.sender);
        dest.writeInt(this.view);
        dest.writeString(this.message);
    }

    public ChatMessage() {
    }

    protected ChatMessage(Parcel in) {
        this.id = in.readLong();
        this.time = in.readLong();
        this.question_id = in.readInt();
        this.sender = in.readInt();
        this.view = in.readInt();
        this.message = in.readString();
    }

    public static final Parcelable.Creator<ChatMessage> CREATOR = new Parcelable.Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel source) {
            return new ChatMessage(source);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };
}
