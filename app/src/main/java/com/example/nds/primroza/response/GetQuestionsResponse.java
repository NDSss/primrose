package com.example.nds.primroza.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.nds.primroza.classes.MainProducts;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.classes.ProductList;
import com.example.nds.primroza.classes.Question;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
@JsonAdapter(GetQuestionsResponse.GetTemplateDeserializer.class)
public class GetQuestionsResponse implements Parcelable {
    private ArrayList<Question> questions;

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.questions);
    }

    public GetQuestionsResponse() {
    }

    protected GetQuestionsResponse(Parcel in) {
        this.questions = in.createTypedArrayList(Question.CREATOR);
    }

    public static final Creator<GetQuestionsResponse> CREATOR = new Creator<GetQuestionsResponse>() {
        @Override
        public GetQuestionsResponse createFromParcel(Parcel source) {
            return new GetQuestionsResponse(source);
        }

        @Override
        public GetQuestionsResponse[] newArray(int size) {
            return new GetQuestionsResponse[size];
        }
    };

    public static class GetTemplateDeserializer implements JsonDeserializer<GetQuestionsResponse> {

        @Override
        public GetQuestionsResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            GetQuestionsResponse getQuestionsResponse = new GetQuestionsResponse();
            ArrayList<Question> questions = new ArrayList<>();
            for(JsonElement obj:((JsonArray) json)){
                questions.add(context.deserialize(obj,Question.class));
            }
            getQuestionsResponse.setQuestions(questions);
            return getQuestionsResponse;
        }
    }
}
