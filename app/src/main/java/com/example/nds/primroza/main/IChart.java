package com.example.nds.primroza.main;

import com.example.nds.primroza.classes.Product;

public interface IChart {
    void addItem();
    void removeItem();
}
