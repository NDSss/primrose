package com.example.nds.primroza.main;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.nds.primroza.R;

public class BottomNavigationHelper {

    public static void setNotification(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, int value){
        if(value>0){
            removeBadge(bottomNavigationView,itemId);
            showBadge(context,bottomNavigationView,itemId,String.valueOf(value));
        } else {
            removeBadge(bottomNavigationView,itemId);
        }
    }

    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.layout_news_bage, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);
        text.setText(value);
        itemView.addView(badge);
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }
}
