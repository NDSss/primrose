package com.example.nds.primroza.products.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.Actions;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.app.Urls;
import com.example.nds.primroza.base.BaseEditDialog;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.IBaseEditDialog;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.base.events.EventsActions;
import com.example.nds.primroza.classes.Product;
import com.example.nds.primroza.di.ActivityComponent;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderActivity extends BaseLoadingActivity implements IBaseEditDialog {

    public static final String PRODUCT = "OrderActivity.PRODUCT";
    public static final int TYPE_PHONE = 0;
    public static final int TYPE_ADRESS = 1;
    public static final int TYPE_EXTRA_INF = 2;
    public static final int TYPE_DELIVERY_TIME = 3;

    @BindView(R.id.iv_activity_order_back)
    ImageView ivActivityOrderBack;
    @BindView(R.id.tv_activity_order_back)
    TextView tvBack;
    @BindView(R.id.tv_activity_order_sum)
    TextView tvSum;
    @BindView(R.id.sdw_activity_order)
    SimpleDraweeView sdwActivityOrder;
    @BindView(R.id.et_activity_order_reciver_person)
    EditText etActivityOrderReciverPerson;
    @BindView(R.id.et_activity_order_reciver_adress)
    EditText etActivityOrderReciverAdress;
    @BindView(R.id.et_activity_order_reciver_inf)
    EditText etActivityOrderReciverInf;
    @BindView(R.id.et_activity_order_reciver_time)
    EditText etActivityOrderReciverTime;
    @BindView(R.id.btn_activity_order_chart)
    Button btnActivityOrderChart;
    @BindView(R.id.btn_activity_order_order)
    Button btnActivityOrderOrder;
    private Product product;
    private EditText editingEt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        if(getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }
        if(getActionBar()!=null){
            getActionBar().hide();
        }
        ButterKnife.bind(this);
        btnActivityOrderChart.setOnClickListener(v->toChart());
        btnActivityOrderOrder.setOnClickListener(v->goToMain());
        ivActivityOrderBack.setOnClickListener(v->finish());
        tvBack.setOnClickListener(v->finish());
        etActivityOrderReciverPerson.setOnClickListener(v->showEditDialog(TYPE_PHONE,(EditText) v));
        etActivityOrderReciverAdress.setOnClickListener(v->showEditDialog(TYPE_ADRESS,(EditText) v));
        etActivityOrderReciverInf.setOnClickListener(v->showEditDialog(TYPE_EXTRA_INF,(EditText) v));
        etActivityOrderReciverTime.setOnClickListener(v->showEditDialog(TYPE_DELIVERY_TIME,(EditText) v));
        if(getIntent().hasExtra(PRODUCT)){
            product = getIntent().getParcelableExtra(PRODUCT);
            sdwActivityOrder.setImageURI(Urls.FULL_IMAGE+product.getPhotos().get(0));
        }
        tvSum.setText("Сумма заказа "+product.getPrice());
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_order;
    }

    private void goToMain(){
        setResult(RESULT_OK);
        finish();
    }

    private void toChart(){
        dataManager.addToChart(product);
        App.get(this).getEventBus().notifyEvent(new Event(EventsActions.ADD_TO_CART));
        setResult(Actions.RESULT_PREVIEW_TO_FAVORITE);
        finish();
    }

    private void showEditDialog(int type,EditText et){
        BaseEditDialog dialog;
        switch (type){
            case TYPE_PHONE:
                dialog = OrderPersonEditDialog.newInstance("NDS","+7(987)654-32-10");
                break;
            case TYPE_ADRESS:
                dialog = OrderEditDialog.newInstance(et.getHint().toString(),et.getText().toString());
                break;
            case TYPE_EXTRA_INF:
                dialog = OrderEditDialog.newInstance(et.getHint().toString(),et.getText().toString());
                break;
            case TYPE_DELIVERY_TIME:
                dialog = OrderEditDialog.newInstance(et.getHint().toString(),et.getText().toString());
                break;
            default:
                dialog = OrderEditDialog.newInstance(et.getHint().toString(),et.getText().toString());
                break;
        }
        dialog.setEditDialog(this::edit);
        editingEt = et;
        dialog.show(getSupportFragmentManager(),dialog.getTag());
    }

    @Override
    public void edit(String data) {
        editingEt.setText(data);
    }

    @Override
    public void onEvent(Event event) {

    }
}
