package com.example.nds.primroza.support;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.nds.primroza.R;
import com.example.nds.primroza.app.App;
import com.example.nds.primroza.base.BaseLoadingActivity;
import com.example.nds.primroza.base.IBaseEditDialog;
import com.example.nds.primroza.base.events.Event;
import com.example.nds.primroza.classes.Question;
import com.example.nds.primroza.di.ActivityComponent;
import com.example.nds.primroza.products.order.OrderEditDialog;
import com.example.nds.primroza.response.CreateQuestionResponce;
import com.example.nds.primroza.response.GetQuestionsResponse;
import com.example.nds.primroza.response.LoginResponse;
import com.example.nds.primroza.servises.IProductServise;

import butterknife.BindView;
import retrofit2.Callback;
import retrofit2.Response;

public class SupportQuestionsActivity extends BaseLoadingActivity implements QuestionsAdapter.IQuestionClickedListener {
    @BindView(R.id.btn_activity_question_add)
    Button btnAdd;
    @BindView(R.id.rv_activity_questions)
    RecyclerView rvQuestions;

    private QuestionsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Поддержка");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        adapter = new QuestionsAdapter();
        adapter.setListener(this::questionClocked);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvQuestions.setLayoutManager(manager);
        rvQuestions.setAdapter(adapter);
        btnAdd.setOnClickListener(v->setQuestionName());
//        llQuestion.setOnClickListener(v->startActivity(new Intent(this,supportChatActivity.class)));
        getQuestioons();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setQuestionName(){
        OrderEditDialog dialog = OrderEditDialog.newInstance("Тема",null);
        dialog.setEditDialog(new IBaseEditDialog() {
            @Override
            public void edit(String data) {
                createQuestion(data);
            }
        });
        dialog.show(getSupportFragmentManager(),"questionName");
    }

    private void createQuestion(String questionName){
//        startLoading();
        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<CreateQuestionResponce> weatherResponceCall = productServise.createQuestion(30,dataManager.getLogin().getId(),questionName);
        weatherResponceCall.enqueue(new Callback<CreateQuestionResponce>() {
            @Override
            public void onResponse(retrofit2.Call<CreateQuestionResponce> call, Response<CreateQuestionResponce> response) {
                getQuestioons();
            }

            @Override
            public void onFailure(retrofit2.Call<CreateQuestionResponce> call, Throwable t) {
                handleError(t.getMessage());
            }
        });
    }

    private void getQuestioons(){
//        startLoading();
        IProductServise productServise = App.get(this).getRetrofit().create(IProductServise.class);
        final retrofit2.Call<GetQuestionsResponse> weatherResponceCall = productServise.getQuestions(dataManager.getLogin().getApiKey());
        weatherResponceCall.enqueue(new Callback<GetQuestionsResponse>() {
            @Override
            public void onResponse(retrofit2.Call<GetQuestionsResponse> call, Response<GetQuestionsResponse> response) {
                handleGetQuestions(response.body());
//                completeLoading();
            }

            @Override
            public void onFailure(retrofit2.Call<GetQuestionsResponse> call, Throwable t) {
                handleError(t.getMessage());
            }
        });
    }
    private void handleError(String message){
        Toast.makeText(this,message,Toast.LENGTH_LONG);
    }

    private void handleGetQuestions(GetQuestionsResponse response){
        adapter.setQuestions(response.getQuestions());
    }

    private void handleCreateQuestion (CreateQuestionResponce responce){

    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_questions;
    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void questionClocked(Question question) {
        Intent intent = new Intent(this,supportChatActivity.class);
        intent.putExtra(supportChatActivity.QUESTION,question);
        startActivity(intent);
    }
}
