package com.example.nds.primroza.classes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Product implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("id_group")
    private String id_group;
    @SerializedName("id_product")
    private String id_product;
    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private String price;
    @SerializedName("photos")
    private ArrayList<String> photos;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("description")
    private String description;
    private boolean isFavorite = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_group() {
        return id_group;
    }

    public void setId_group(String id_group) {
        this.id_group = id_group;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.id_group);
        dest.writeString(this.id_product);
        dest.writeString(this.title);
        dest.writeString(this.price);
        dest.writeStringList(this.photos);
        dest.writeString(this.startDate);
        dest.writeString(this.endDate);
        dest.writeString(this.description);
        dest.writeByte(this.isFavorite ? (byte) 1 : (byte) 0);
    }

    public Product() {
    }

    protected Product(Parcel in) {
        this.id = in.readString();
        this.id_group = in.readString();
        this.id_product = in.readString();
        this.title = in.readString();
        this.price = in.readString();
        this.photos = in.createStringArrayList();
        this.startDate = in.readString();
        this.endDate = in.readString();
        this.description = in.readString();
        this.isFavorite = in.readByte() != 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
