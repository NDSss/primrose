package com.example.nds.primroza.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nds.primroza.R;


/**
 * Created by maxmobiles on 03.12.2017.
 */

public class ViewProgressView extends RelativeLayout {

    public ViewProgressView(Context context) {
        super(context);
        init(context);
    }

    public ViewProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ViewProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.progress_view, this);
        setClickable(true);
    }

    public void startLoading() {
        this.setVisibility(VISIBLE);
    }

    public void completeLoading() {
        this.setVisibility(GONE);
    }
}
