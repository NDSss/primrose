package com.example.nds.primroza.base.eventbus;

import com.example.nds.primroza.base.events.Event;

import java.util.ArrayList;

public class EventBusController implements IEventBusSubject {

    private ArrayList<IEventBusObserver> observers;

    public EventBusController(){
        observers = new ArrayList<>();
    }

    @Override
    public void addObserver(IEventBusObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(IEventBusObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyEvent(Event event) {
        for(IEventBusObserver observer:observers){
            observer.onEvent(event);
        }
    }
}
