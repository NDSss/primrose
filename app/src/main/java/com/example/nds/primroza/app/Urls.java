package com.example.nds.primroza.app;

public class Urls {
    public static final String BASE_URL = "http://back.marketdev.beget.tech/backend/api/";
    public static final String product = "product";
    public static final String FULL_IMAGE = "http://front.marketdev.beget.tech/uploads/";
    public static final String city = "product/get-city";
    public static final String LOGIN = "user/login";
    public static final String GET_QUESTIONS = "chat/get-questions";
    public static final String GET_MESSAGES = "chat/get-message";
    public static final String SEND_MESSAGE = "chat/send-message";
    public static final String CREATE_QUESTION = "chat/create-question";
    public static final String SUBSCRIBE_PUSHES = "chat/subscribe-pushes";
}
