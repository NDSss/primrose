package com.example.nds.primroza.map.callback;

import com.example.nds.primroza.map.view.MapFragment;

public interface IAtmCallback {

    void onAttach(MapFragment mapFragment);

    void onRetry();
}
