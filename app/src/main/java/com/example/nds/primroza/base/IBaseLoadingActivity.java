package com.example.nds.primroza.base;

public interface IBaseLoadingActivity {
    void startLoading();
    void completeLoading();
    void errorLoading(String message);
}
